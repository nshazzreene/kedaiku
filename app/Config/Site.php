<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class Site extends BaseConfig
{
    public $defaultTitle  = 'DMBNY STORE';
    //public $siteEmail = 'webmaster@example.com';
    public $siteName = 'DMBNY SERVICE ';
    public $siteDescription = 'ONLY FINEST QUALITY FOR OUR CUSTOMERS';
}
