<?php

namespace App\Models;

use CodeIgniter\Model;

class OrdersModel extends Model
{
    protected $table      = 'orders';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['full_name','email', 'order_no', 'no_phone', 'address_1', 'address_2', 
        'poscode','district', 'country', 'total_amount', 'amount', 'items', 'status'];

    // Dates
   protected $useTimestamps = true;
   protected $dateFormat    = 'datetime';
   protected $createdField  = 'created_at';
   protected $updatedField  = 'updated_at';
   protected $deletedField  = 'deleted_at';

    // Validation
 //   protected $validationRules      = [];
 //   protected $validationMessages   = [];
 //   protected $skipValidation       = false;
 //   protected $cleanValidationRules = true;

    // Callbacks
 //   protected $allowCallbacks = true;
 //   protected $beforeInsert   = [];
 //   protected $afterInsert    = [];
 //   protected $beforeUpdate   = [];
 //   protected $afterUpdate    = [];
 //   protected $beforeFind     = [];
 //   protected $afterFind      = [];
 //   protected $beforeDelete   = [];
 //   protected $afterDelete    = [];

 
}