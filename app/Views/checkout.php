<?= $this->extend('templates/front_layout') ?>

<?= $this->section('main-content') ?>

    <h3>CHECKOUT</h3>

    <div class="row">
      <div class="col-12">
        <table class="table table-striped">
          <thead>
            <tr>
              <th> </th>
              <th>Product</th>
              <th>Price</th>
              <th width="15%">Amount</th>
              <th>Total</th>
            </tr>
          </thead>

          <tbody>
            <?php if(isset($_SESSION['cart']['items']) && (  count($_SESSION['cart']['items']) > 0 ) ) : ?>                
              <?php $counter = 0; ?>
              <?php $total_amount = 0; ?>
              <?php foreach( $_SESSION['cart']['items'] as $item) : ?>
                <tr>
                  <td><?= ++$counter;?></td>
                  <td><?= $item['nama'] ?></td>
                  <td><?= number_format( $item['harga'], 2) ?></td>
                  <td><?= $item['amount']?></td>
                  <td>RM <?= number_format($item['harga'] * $item['amount'], 2)?></td>
                </tr>
                <?php $total_amount += ( $item['harga'] * $item['amount'] ); ?>
              <?php endforeach; ?>
                <tr>
                  <td align="right" colspan="4"><strong>Total Amount</strong></td>
                  <td><strong>RM <?= number_format( $total_amount ,2); ?></strong></td>
                </tr>
              <?php else : ?>

                <tr>
                  <td colspan="5" class="text-center" >
                  Your cart is empty
                  </td>
                </tr>
              <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
<hr>

<?php if (isset($_SESSION['form_failed'])) :?>
            <div class="row">
                <div class="col">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Invalid data!</strong> Please complete form with valid information.</a>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>

<?php endif; ?>

<form action="/checkout" method="POST">


<?php
  $full_name_invalid = (isset($_SESSION['form_errors']['full_name'])) ? 'is-invalid' : '';
  $full_name_value = (isset($_SESSION['form_data']['full_name'])) ? $_SESSION['form_data']['full_name'] : '';
?>
<div class="form-group">
  <label for="full_name">Full Name</label>
  <input type="text" class="form-control <?= $full_name_invalid?>" name="full_name" id="full_name" value="<?= $full_name_value?>">
</div>

<div class="row">

  <?php
    $email_invalid = (isset($_SESSION['form_errors']['email'])) ? 'is-invalid' : '';
    $email_value = (isset($_SESSION['form_data']['email'])) ? $_SESSION['form_data']['email'] : '';
  ?>

  <div class="form-group col-sm-6">
    <label for="email">Email</label>
    <input type="email" class="form-control <?= $email_invalid?>" name="email" id="email" value="<?= $email_value?>">
  </div>

  <?php
    $no_phone_invalid = (isset($_SESSION['form_errors']['no_phone'])) ? 'is-invalid' : '';
    $no_phone_value = (isset($_SESSION['form_data']['no_phone'])) ? $_SESSION['form_data']['no_phone'] : '';
  ?>

  <div class="form-group col-sm-6">
    <label for="email">No Phone</label>
    <input type="text" class="form-control <?= $no_phone_invalid?>" name="no_phone" id="no_phone" value="<?=$no_phone_value?>">
  </div>
    
</div>

<div class="form-group">
  <?php
    $address_1_invalid = (isset($_SESSION['form_errors']['address_1'])) ? 'is-invalid' : '';
    $address_1_value = (isset($_SESSION['form_data']['address_1'])) ? $_SESSION['form_data']['address_1'] : '';
    $address_2_value = (isset($_SESSION['form_data']['address_2'])) ? $_SESSION['form_data']['address_2'] : '';
    ?>
    <label for="address_1">Address</label>
    <input type="text" class="form-control mb-2 <?= $address_1_invalid?>" name="address_1" id="address_1" value="<?= $address_1_value?>">
    <input type="text" class="form-control" name="address_2" id="address_2" value="<?= $address_2_value?>">
  </div>

  <div class="row">
    <?php
    $poscode_invalid = (isset($_SESSION['form_errors']['poscode'])) ? 'is-invalid' : '';
    $poscode_value = (isset($_SESSION['form_data']['poscode'])) ? $_SESSION['form_data']['poscode'] : '';
    ?>       
    <div class="form-group col-sm-2">
      <label for="poscode">Poscode</label>
      <input type="text" class="form-control <?= $poscode_invalid?>" name="poscode" id="poscode" value="<?= $poscode_value?>">
    </div>

    <div class="form-group col-sm-6">
    <?php
    $district_invalid = (isset($_SESSION['form_errors']['district'])) ? 'is-invalid' : '';
    $district_value = (isset($_SESSION['form_data']['district'])) ? $_SESSION['form_data']['district'] : '';
    ?>       
    <label for="district">District</label>
      <input type="text" class="form-control <?= $district_invalid?>" name="district" id="district" value="<?=$district_value?>">
    </div>

    <div class="form-group col-sm-4">
        <label for="country">Country</label>
        <?php
    $negeri_invalid = (isset($_SESSION['form_errors']['country'])) ? 'is-invalid' : '';
    $negeri_value = (isset($_SESSION['form_data']['country'])) ? $_SESSION['form_data']['country'] : null;
    ?>
      <?= form_dropdown('country', $negeri, $negeri_value, [ 'class' => 'form-control '.$negeri_invalid ]); ?>
    </div>
  </div>


<button type="submit" class="btn btn-primary" name="payment_gateway" value="securepay">Online Payment</button>

<button type="submit" class="btn btn-primary" name="payment_gateway" value="stripe">Pay Using Stripe</button>

</form>


<?= $this->endSection() ?>
