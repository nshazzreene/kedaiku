<?= $this->extend('templates/admin_layout') ?>

<?= $this->section('main-content') ?>

  <div class="container mt-5">

    <?php if (isset($_SESSION['success'])) :?>
      <div class="row">
        <div class="col">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> New data has been added
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
             
          </div>
        </div>
      </div>
   <?php endif; ?>

   <?php if (isset($_SESSION['deleted'])) :?>
      <div class="row">
        <div class="col">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> Data has been deleted.
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>            
          </div>
        </div>
      </div>
   <?php endif; ?>


  <div class="row">
    <div class="col-12">
      <a href = "/produk/add"   class="btn btn-sm btn-primary float-right">Add New</a>
      <h3>PRODUCT</h3>
    </div>
            
    <div class="col-12">

        <table class="table table-striped table-sm">
          <thead class="table-dark ">
             <tr>
                <th>ID</th>
                <th>IMAGE</th>
                <th>NAME</th> 
                <th>CATEGORY</th> 
                <th>PRICE</th>
                <th></th>
             </tr>
          </thead>
          <tbody>

          <?php $counter = 0; ?>
          <?php foreach ($produk as $g) :?>
    <tr>
        <td><?= ++$counter;?></td>
        <td>
            <img class="gambar-kucing" src="/img/produk/<?= $g['gambar']?>" alt="">
        </td>
        <td><?= $g['nama']?></td>
        <?php if (isset($g['kategori_id']) && isset($kategori[$g['kategori_id']])) : ?>
            <td><?= $kategori[$g['kategori_id']]?></td>
        <?php else : ?>
            <td>Unknown Category</td>
        <?php endif; ?>
        <td>RM <?= number_format($g['harga'], 2)?></td>
        <td>
            <a href="/produk/edit/<?= $g['id'];?>" class="btn btn-sm btn-primary">EDIT</a>
            <a href="/produk/delete/<?= $g['id'];?>" class="btn btn-sm btn-danger">DELETE</a>
        </td>
    </tr>
<?php endforeach; ?>
               
          </tbody>
        </table>
        
        <div id="my-pagination">
          <?= $pager->links() ?>  
        </div>  
  </div>
  </div>
  </div>
  
        
  <script>
    function confirm_delete (id) {
      if (confirm('Are you sure want to delete this?'+ id +'?')); {
        window.location href = '/produk/delete/'+id;
      }
    }
  </script> 

<?= $this->endSection() ?>
