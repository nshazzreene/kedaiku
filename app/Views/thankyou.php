<?= $this->extend('templates/front_layout') ?>

<?= $this->section('main-content') ?>

<h3>THANK YOU FOR YOUR PURCHASE</h3>

<div class="row">
    <div class="class-12">           
        <table class="table table-striped">
            <thead>
                <tr>
                 <th></th>
                 <th>Product</th>
                 <th>Price</th>  
                 <th width="15%">Amount</th> 
                 <th>Total</th>              
                </tr>
            </thead>
            <tbody>

            <?php $counter = 0; ?>
            <?php foreach ($order_items as $item) : ?>                  
            <tr>
                <td><?= ++$counter;?></td>
                <td><?= $produk[$item['produk_id']]['nama'] ?></td>
                <td><?= number_format ($item['harga'], 2) ?></td>
                <td><?= $item['amount']?></td>
                <td>RM <?= number_format ($item['harga'] *  $item ['amount'], 2)?></td>
            </tr>

            <?php endforeach; ?>  
            <tr>
                <td align="right" colspan="4"><strong>Total Amount</strong></td>
                <td><strong>RM <?= number_format($order['total_amount'],2); ?></strong></td>
            </tr> 

            </tbody>
        </table>
    </div>
</div>



<?= $this->endSection() ?>