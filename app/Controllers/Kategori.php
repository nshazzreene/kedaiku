<?php
  
  namespace App\Controllers;

  class Kategori extends BaseController
  {

   private $session; // Declare the session variable
 
    function __construct()
    {
        $this->session = session();
       //$this->session= \Config\Services::session();
       $this->kategori_model = new \App\Models\KategoriModel();
       
    }
    
    public function index()
     {
         $data = 
          [
              'kategori' => $this->kategori_model->orderBy('id','desc')->paginate(5),
              'pager' => $this->kategori_model->pager,
          ];
    
        return view('admin_kategori/listing',$data);
      }

    function edit($id)
     {
        $kategori_model = new \App\Models\KategoriModel();

       // helper('form');
         $kategori = $this->kategori_model->find($id);

         return view('admin_kategori/edit', [ 
            'kategori' => $kategori
        ]);
     }

     public function save_edit($id) // Use 'public' instead of 'function'
     {

        $produk_model = new \App\Models\ProdukModel();
         $data = [
             'nama' => $this->request->getPost('nama'),
             'description' => $this->request->getPost('description'),
             'harga' => $this->request->getPost('harga')
        ];
 
        $this->kategori_model->update($id, $data);

        $_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/kategori/edit/'. $id);

     }
 
    function delete ($id)
    {                  
        $kategori_model = new \App\Models\KategoriModel();
        
        // cari based on id then delete
        $this->kategori_model->where('id',$id)->delete();

        $_SESSION['deleted']=true;
        $this->session->markAsFlashdata('deleted');

        return redirect()->back();
    }

    function add()
    {    
       return view ('kategori_produk/add');
    }

    public function save_new()
    {
        $data = [
            'nama' => $this->request->getPost('nama'),
            'description' => $this->request->getPost('description'),
            'harga' => $this->request->getPost('harga')
        ];
     
        $this->kategori_model->insert($data);
     
        $_SESSION['success'] = true;
        $this->session->markAsFlashdata('success');
     
        return redirect()->to('/kategori');
    }
    
}