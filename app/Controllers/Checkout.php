<?php
  
namespace App\Controllers;

class Checkout extends BaseController
{ 
  var $negeri = [
      '' => '[ -- Choose your Country -- ]',
      'WP Kuala Lumpur' => 'WP Kuala Lumpur',
      'WP Putrajaya' => 'WP Putrajaya',
      'WP Labuan' => 'WP Labuan',
      'Selangor' => 'Selangor',
      'Perak' => 'Perak',
      'Kedah' => 'Kedah',
      'Pulau Pinang' => 'Pulau Pinang',
      'Perlis' => 'Perlis',
      'Kelantan'=> 'Kelantan',
      'Terengganu' => 'Terengganu',
      'Pahang' => 'Pahang',
      'Johor' => 'Johor',
      'Melaka' => 'Melaka',
      'Negeri Sembilan' => 'Negeri Sembilan',
      'Sabah' => 'Sabah',
      'Sarawak' => 'Sarawak'
  ];

  function __construct() 
  {
    $this->data['negeri'] = $this->negeri;
    $this->data['page_title'] = "Checkout";

    $this->session = session();

    $this->orders_model = new \App\Models\OrdersModel();
    $this->order_items_model = new \App\Models\OrderItemsModel();
    $this->payments_model = new \App\Models\PaymentsModel();
  }

  function index () 
  {
    return view ('checkout.php', $this->data);
  }

  private function get_cart_items() {
    if ( isset($_SESSION['cart']['items'])  )  {
      return count($_SESSION['cart']['items']);
    }
    return 0;
  }

  private function get_cart_amount() {
  $amount = 0;

  if (
    isset($_SESSION['cart']['items']) 
    && (count($_SESSION['cart']['items']) > 0) 
    ) {
    foreach($_SESSION['cart']['items'] as $item) {
      $amount += $item['amount'];
    }
    }
    return $amount;
  }

  private function get_cart_total_amount() {
    $total_amount = 0;
    if (
      isset($_SESSION['cart']['items']) 
      && (count($_SESSION['cart']['items']) > 0) 

    ) { foreach($_SESSION['cart']['items'] as $item) {
      $total_amount += ( $item['harga'] * $item['amount'] );
      }
    }
    
    return $total_amount;
  }

  function process_checkout ()
  {
    $validation =  \Config\Services::validation();

    $validation->setRule('full_name', 'Full Name', 'required|min_length[3]');
    $validation->setRule('email', 'Email', 'required|valid_email');
    $validation->setRule('no_phone', 'No Phone', 'required|min_length[10]');
    $validation->setRule('address_1', 'Address 1', 'required|min_length[10]');
    $validation->setRule('district', 'District', 'required|min_length[3]');
    $validation->setRule('poscode', 'Poscode', 'required|exact_length[5]|numeric');
    $validation->setRule('country', 'Country', 'required');

    if ($validation->run($_POST)) {
      helper('text');

      $order_no = random_string('alnum', 10);

      $payment_data = [
        'full_name' => $this->request->getPost('full_name'),
        'email'=> $this->request->getPost('email'), 
        'order_no'=> $order_no,
        'no_phone' => $this->request->getPost('no_phone'),
        'address_1' => $this->request->getPost('address_1'),
        'address_2' => $this->request->getPost('address_2'), 
        'poscode' => $this->request->getPost('poscode'),
        'district' => $this->request->getPost('district'),
        'country' => $this->request->getPost('country'), 
        'total_amount' => $this->get_cart_total_amount(), 
        'amount'=> $this->get_cart_amount(),
        'items' => $this->get_cart_items(),
        'status' => 'pending payment'
     ];

     $order_id = $this->orders_model->insert($payment_data);
      // $qty = $this->request->getPost('qty');

    $payment_data['order_id'] = $order_id;
    $payment_data['items'] = [];
       
   foreach( $_SESSION['cart']['items'] as $item ) {
     $order_item = [
      'order_id' => $order_id,
      'produk_id' => $item['id'],
      'harga' => $item['harga'],
      'amount' => $item['amount']
     ];

     $payment_data['items'][] = $order_item;
     $this->order_items_model->insert($order_item);

    }

    $payment_gateway = $this->request->getPost('payment_gateway');
    $this->payments_model->insert([
    'payment_gateway' => $payment_gateway,
    'data' => date('Y-m-d H:i:s')."\n".json_encode($payment_data)."\n",
    'order_no' => $order_no,
    'status' => 'pending'
    ]);

    $payment_data['redirect_url'] = site_url('checkout/redirect/'.$payment_gateway);
    $payment_data['callback_url'] = site_url('checkout/callback/'.$payment_gateway);

    switch($payment_gateway) {
      case 'securepay' :
        $sp = new \App\Libraries\Payments\SecurePay('sandbox');
        $sp->go($payment_data);
      break;
  }

    } else {
 
    $_SESSION['form_data'] = $_POST;
    $_SESSION['form_errors'] = $validation->getErrors();
    $_SESSION['form_failed'] = true;

    $this->session->markAsFlashdata('form_data');    
    $this->session->markAsFlashdata('form_errors');    
    $this->session->markAsFlashdata('form_failed');    

    return redirect()->back();

    }
  }

  function redirect ($payment_gateway = '') {
  {
    if ($payment_gateway == '')
    throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
  }
    
  switch ($payment_gateway) {
    case 'securepay' :
      $sp = new \App\Libraries\Payments\Securepay('development');
      $order_no = $this->request->getPost('order_number');
      $payment_status = $this->request->getPost('payment_status');
    break;
  }

  if ($sp->verify()) {

    $payment = $this->payments_model->where('order_no', $order_no )->first();
    $order = $this->orders_model->where('order_no', $order_no )->first();

    if($payment_status == 'true') {
        $payment['status'] = 'success';
        $order['status'] = 'success';
    } else {
        $payment['status'] = 'failed';
        $order['status'] = 'failed';
    }

    $payment['data'] .= "\n".date('Y-m-d H:i:s')."\n".json_encode($_POST)."\n";

    $this->payments_model->update($payment['id'], $payment);
    $this->orders_model->update($order['id'], $order);
    
  } else {
    throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
  }

  if ($payment_status == 'true') {
    return redirect()->to('/checkout/thankyou/'.$order_no);
  } 

  return redirect()->to('/cart');
  }


  function thankyou ($order_no = '') 
  {
    $this->data['page_title'] = "Thank you for your purchase";
    
    if ($order_no == '') {
      throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    $this->data['order'] = $this->orders_model->where('order_no', $order_no )->first();
    
    if ((!$this->data['order']) || ($this->data['order']['status'] != 'success')) {
      throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    $this->data['order_items'] = $this->order_items_model->where('order_id', $order_no['id'] )->findAll();
   
    $produk_ids = [];
    foreach ($this->data['order_items'] as $items) {
      $produk_ids[] = $items ;['produk_id'];
    }

    $produk_model = new \App\Models\ProdukModel();

    $produks = $produk_model->find($produk_ids);

    $produk2 = [];
    foreach($produks as $p) {
      $produk2[ $p['id'] ] = $p;
    }

    $this->data['produk'] = $produk2;

    return view('thankyou', $this->data);

  }

}
