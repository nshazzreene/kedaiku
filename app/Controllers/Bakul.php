<?php
  
namespace App\Controllers;

class Bakul extends BaseController
{
  function __construct() 
  {
    $this->produk_model = new \App\Models\ProdukModel();
    $this->session = session();
    //$this->session= \Config\Services::session();
  }

  //bakul -- display item dlm cart
  public function index ()
  { 
    return view ('bakul');
  }

  //add to cart --receive POST data
  function add()
  { 
    // get data from form
    $produk_id =  $this->request->getPost('produk_id');
    $amount =  $this->request->getPost('amount');
    
    // find product from databse
    $produk = $this->produk_model->find($produk_id);

    // if product found, add to cart
    if ($produk){
      $this->add_cart($produk['id'], $produk['nama'], $produk['harga'], $amount); 
      $_SESSION['success']=true;
      $this->session->markAsFlashdata('success');  
    }
      return redirect()->back();
  }

  function remove($id)
  {
    foreach($_SESSION['cart']['items'] as $k => $item)
    {
      if ($item['id'] == $id) {
        unset ($_SESSION['cart']['items'][$k]);
      }
    }

    $_SESSION['success']=true;
    $this->session->markAsFlashdata('success'); 
    return redirect()->back(); 
  }
    
  function update()
  {
    $_SESSION['cart']=[
      'items'=>[]
    ];

    $amount = $this->request->getPost('amount');

    if (is_array($amount)) {
      $all_ids = array_keys($amount);

      $produks = $this->produk_model->find( $all_ids );

    foreach($produks as $produk) 
    {
      if ($amount[$produk['id']]> 0) {
      $this->add_cart($produk['id'], $produk['nama'], $produk['harga'], $amount[$produk['id'] ] );
      }

    }

    $_SESSION['success']=true;
    $this->session->markAsFlashdata('success');

    } else {
    $_SESSION['empty'] = true;
    $this->session->markAsFlashdata('empty'); 

    }

    return redirect()->back();
  }

  // add item to session cart
  protected function add_cart( $id, $nama, $harga, $amount )
  { 
    if (!isset($_SESSION['cart']['items'])) 
    {
      $_SESSION['cart']=[
        'items'=>[]
      ];
    }

    // see if item already in caart
    $found = false;
    foreach($_SESSION['cart']['items'] as $index => $item) {
      if ($item['id'] == $id) {
        $_SESSION['cart']['items'][$index]['amount'] += $amount;
        $found = true;
      }
    }

    // process if item never added to cart
    if (!$found) {
      $_SESSION['cart']['items'][] = [
        'id' => $id,
        'nama' => $nama,
        'harga' => $harga,
        'amount' => $amount     
      ];
    }

    return true;
  }

}