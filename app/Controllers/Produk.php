<?php
  
  namespace App\Controllers;

  class Produk extends BaseController
  {

   private $session; // Declare the session variable
 
    //var $produk_img_location='img/produk';

    function __construct()
    {
        $this->session = session();
       //$this->session= \Config\Services::session();
       $this->produk_model = new \App\Models\ProdukModel();
       $this->kategori_model = new \App\Models\KategoriModel();
       
    }

    public function homepage()
    {
        $kategori = $this->kategori_model->dropdown();
    
        $data = 
        [
            'produk' => $this->produk_model->orderBy('id','desc')->paginate(5),
            'pager' => $this->produk_model->pager,
            'kategori' => $kategori
        ];

        // dd($kategori);
  
        return view('produk_homepage',$data);

    }

    public function index()
    {
        $kategori = $this->kategori_model->dropdown();

        $data = 
        [
         'produk' => $this->produk_model->orderBy('id','desc')->paginate(5),
         'pager' => $this->produk_model->pager,
         'kategori' => $kategori
       ];
    
        return view('admin_produk/listing',$data);
    }

    function edit($id)
{
    $produk_model = new \App\Models\ProdukModel();
    $kategori_model = new \App\Models\KategoriModel(); // Define the kategori_model

    $produk = $produk_model->find($id);
    $kategori = $kategori_model->dropdown();

    return view('admin_produk/edit', [
        'kategori' => $kategori,
        'produk' => $produk
    ]);
}


    public function save_edit($id) // Use 'public' instead of 'function'
    {

        $produk_model = new \App\Models\ProdukModel();
        $data = [
            'nama' => $this->request->getPost('nama'),
            'description' => $this->request->getPost('description'),
            'harga' => $this->request->getPost('harga')
        ];

        if ($this->request->getPost('kategori_id') != '0')
        {
          $data ['kategori_id'] =  $this->request->getPost('kategori_id');
        }
 
        $files = $this->request->getFiles('gambar');
 
        if ($files['gambar']->isValid()) {
            $file = $files['gambar']; // Use array access to get the file
 
             // Generate new secure name
            $file_gambar = $file->getRandomName();
 
             // Move file to its new home
            $file->move('img/produk/', $file_gambar);
 
            $data['gambar'] = $file_gambar;
        }
        $produk_model->update($id, $data);

        return $this->index();
    }
 
    public function delete ($id)
        {                  
          $produk_model = new \App\Models\ProdukModel();
        
          // cari based on id then delete
          $produk_model->where('id',$id)->delete();

          $_SESSION['deleted']=true;
          $this->session->markAsFlashdata('deleted');

          return redirect()->back();
        }


        function add() {		

            $kategori = $this->kategori_model->dropdown();
    
            return view('admin_produk/add', [ 'kategori' => $kategori ]);
        }
    

    public function save_new()
    {
        $data = [
            'nama' => $this->request->getPost('nama'),
            'description' => $this->request->getPost('description'),
            'harga' => $this->request->getPost('harga')
        ];

        if ($this->request->getPost('kategori_id') != '0')
        {
            $data ['kategori_id'] =  $this->request->getPost('kategori_id');
        }
     
        $files = $this->request->getFiles('gambar');
     
        if ($files) {
            $file = $files['gambar'];
     
            // Generate new secure name
            $file_gambar = $file->getRandomName();
     
            // Move file to its new home
            $file->move('img/produk/', $file_gambar);
     
            $data['gambar'] = $file_gambar;
            }
     
        $this->produk_model->insert($data);
     
        $_SESSION['success'] = true;
        $this->session->markAsFlashdata('success');
     
        return redirect()->to('/produk');
         
    }
}